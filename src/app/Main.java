package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main.fxml"));
        Parent root = fxmlLoader.load();
        ((MainCtrl) fxmlLoader.getController()).setStage(primaryStage);
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Planification");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
